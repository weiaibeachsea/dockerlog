package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
)

func main() {
	WriteLog()
	strs := SortByTimeStamp()
	WriteLogByTimeOrderer(strs)
	// b, err := ioutil.ReadAll(i.Read())
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// b := new(bytes.Buffer)
	// b.Write()
	// fmt.Println(b.String()[8:])
	// fmt.Println(b.String())

	// fmt.Printf("%s", b.String())
	// _, err = f.WriteString(b.String())
	// if err != nil {
	// 	fmt.Println("????")
	// 	log.Fatal(err)
	// }

	// dir, err := ioutil.ReadDir("../")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// for _, file := range dir {
	// 	fmt.Println(file.Name())
	// }

	// b, err = ioutil.ReadFile("./main.go")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Printf("%s", b)
	// hdr := make([]byte, 8)
	// for {
	//     _, err := i.Read(hdr)
	//     if err != nil {
	//         log.Fatal(err)
	//     }
	//     var w io.Writer
	//     switch hdr[0] {
	//     case 1:
	//         w = os.Stdout
	//     default:
	//         w = os.Stderr
	//     }
	//     count := binary.BigEndian.Uint32(hdr[4:])
	//     dat := make([]byte, count)
	//     _, err = i.Read(dat)
	//     fmt.Fprint(w, string(dat))
	// }
}

func WriteLog() {
	cli, _ := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	defer cli.Close()
	// var cid []string
	// for {
	// 	var temp string
	// 	fmt.Scanln(&temp)
	// 	if temp == "0" {
	// 		break
	// 	} else {
	// 		cid = append(cid, temp)
	// 	}
	// }
	var cid []string = []string{"peer0.hit", "peer0.iana"}
	f, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	w1 := bufio.NewWriter(f)
	var w2 io.Writer = os.Stderr

	for _, v := range cid {
		i, err := cli.ContainerLogs(context.Background(), v, types.ContainerLogsOptions{
			ShowStderr: true,
			// ShowStdout: true,
			Timestamps: true,
			// Follow:     true,
			Tail: "40",
		})
		if err != nil {
			log.Fatal(err)
		}
		stdcopy.StdCopy(w2, w1, i)

	}
	w1.Flush()
}

func SortByTimeStamp() []string {
	// r := strings.NewReader("Go is a general-purpose language designed with systems programming in mind.")
	// b, err := ioutil.ReadAll(r)
	// if err != nil {
	//     log.Fatal(err)
	// }
	// fmt.Printf("%s", b)

	// dir, err := ioutil.ReadDir("../")
	// if err != nil {
	//     log.Fatal(err)
	// }
	// for _, file := range dir {
	//     fmt.Println(file.Name())
	// }

	fi, err := os.Open("log.txt")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return []string{}
	}
	defer fi.Close()
	var logSet []string
	var timeSet []time.Time
	br := bufio.NewReader(fi)
	for {
		a, _, c := br.ReadLine()
		if c == io.EOF {
			break
		}
		str := strings.Split(string(a), " ")
		tempTime, _ := time.ParseInLocation(time.RFC3339Nano, str[0], time.Local)
		logSet = append(logSet, string(a))
		timeSet = append(timeSet, tempTime)
		fmt.Println(string(a))
	}
	BubbleSort(&logSet, &timeSet)
	return logSet
}

func BubbleSort(logSet *[]string, timeSet *[]time.Time) {
	// func BubbleSort(arr *[5]int){

	// fmt.Println("排序前arr=", (*arr))
	// temp := 0
	for i := 0; i < len(*timeSet); i++ {
		fmt.Println((*timeSet)[i].Format(time.RFC3339Nano))

	}
	for i := 0; i < len(*timeSet)-1; i++ {

		for j := 0; j < len(*timeSet)-1-i; j++ {
			if (*timeSet)[j].After((*timeSet)[j+1]) {
				// if (*arr)[j] > (*arr)[j+1] {
				temp := (*timeSet)[j]
				(*timeSet)[j] = (*timeSet)[j+1]
				(*timeSet)[j+1] = temp

				temp1 := (*logSet)[j]
				(*logSet)[j] = (*logSet)[j+1]
				(*logSet)[j+1] = temp1
			}
		}
	}
	fmt.Println()
	fmt.Println()
	fmt.Println()

	for i := 0; i < len(*timeSet); i++ {
		fmt.Println((*timeSet)[i].Format(time.RFC3339Nano))

	}
	// fmt.Println("排序后arr=", (*arr))
}

func WriteLogByTimeOrderer(strs []string) {
	f2, err := os.OpenFile("timeOrder.log", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fatal(err)

	}
	defer f2.Close()
	for _, v := range strs {
		_, err = f2.Write([]byte(v + "\n"))
		if err != nil {
			log.Fatal(err)

		}
	}

}
